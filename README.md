# Analyzer FIPS Verification

This repository contains a CI configuration to execute FIPS analyser images on a
FIPS-compliant GitLab Runner. This allows us to verify that the FIPS analyser
images are FIPS-compliant.

This is achieved by jobs that run the latest `-fips` image of each analyser on a
runner tagged with `fips` and `aws`. See [this
issue](https://gitlab.com/gitlab-org/gitlab/-/issues/356232#note_1008500182) for
more background on FIPS runners.
